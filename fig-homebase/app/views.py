from django.shortcuts import get_object_or_404
from django.db.models import Q
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
import json
from .serializers import *
from .utils import *
import logging
import django.utils.timezone as timezone
from datetime import datetime


# Create your views here.


class AsdView(APIView):

    def get(self, request, *args, **kwargs):
        """ASD"""
        if True:
            if True:
                data = AsdSerializer().data
                response = dict()
                response['something'] = data
                return Response(response, status=200)
            else:
                return Response({'error': 'No data'}, status=404)
        else:
            return Response({'error': 'not found'}, status=404)
