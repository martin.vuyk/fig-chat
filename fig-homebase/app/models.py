import json
from datetime import datetime, timedelta
from django.db import models
from django.db.models import JSONField

# Create your models here.
from django.utils import timezone


class Asd(models.Model):
    """
    Asd model
    """
    slug = models.SlugField(max_length=100, unique=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    secret_key = models.CharField(max_length=100, unique=True, null=True, blank=True)

    class Meta:
        verbose_name = 'Asd'
        verbose_name_plural = 'Asds'

    def __str__(self):
        return 'Asd ' + self.slug