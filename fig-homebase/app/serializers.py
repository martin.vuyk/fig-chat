from abc import ABC

from rest_framework import serializers
from .models import *
from collections import Iterable


class SourceSerializer(serializers.ModelSerializer):
    asd_field = serializers.SerializerMethodField()

    class Meta:
        model = Asd
        fields = [
            'asd_field'
        ]

    def get_field_name(self, obj):
        return obj.slug
