# Fig-Chat, the chat that is a figment of the user's imagination
## Vue 3 + TypeScript + Vite + Vuetify + GunJS
Inspired by Fireship's video https://www.youtube.com/watch?v=J5x3OMXjgMc

## Intended Architechture
Session stateful chat storage, that gets deleted when both parties close the app.
Decentralized client-side temp-chat-storage with GunJS to ensure that fact.

The ideal would be to create a P2P network with homomorphic encryption and locally-hosted containerized
relay servers with user auth and contact lists. But alas, I am not superman and can't code 
such black magic

### fig-front: 
FrontEnd for the Decentralized App, temp storage of chat content
### fig-webview: 
multi-platform python pyinstaller and pywebview that mounts fig-front with no tracking.
Should implement onion-style routing
### fig-homebase: 
Django Rest & PostgreSQL for user auth, contact list, chat subscription 
list, etc. Through user choise, the port can be hosted and let through their router's Firewall
to be able to log-in from other locations with only the fig-front
#### The whole thing will be able to be packaged with pyinstaller to become portable and locally hosted

### privateAlias: 
Only for login. Can't be changed and is unique
### publicAlias:
Default name in any chat. Can be changed and is unique
### contactAlias: 
name by which that specific contact was established. Can't be changed and isn't unique

### creating a contact:
#### A single use JWT will be created and sent as request:
        {JWT: string, validity: string(UTC), emitted: string(UTC), contactAlias:string}
#### A public key is generated and sent as response upon contact approval:
        {contactAlias: string, approved: string(UTC), hash: string}, 
#### Both parties store the response with the other's contacAlias in their tables

### inviting a contact to a chat:
    hash pertaining to that contact will be broadcasted, where the one that gives the correct response
    (hashed from alias and contact approval date) is added to the chat (they will have to be online)
    and sent a private key for the encryption of that chat

## Features:
#### Enable Anonimous Mode, where one navigates the chatrooms without any sort of backtracing possible. Contact making should still work the same way, but should be stored on localStorage client-side and not in the homebase server
#### Enable Anonimous chatrooms, that do not get stored in the chat subscription homebases of the participants
#### Some sort of queue for chat initialization req. and messages for when the user wasn't online
#### fig-front fetch requests to the GunJS network should be able to be routed through the Tor network
#### Invite only, public, semi-public chatrooms and other auth mechanisms (muting, banning, etc.)

# DISCLAIMER
This project is a proof of concept, no promise to have it production-ready nor complete any time soon