import { MessageType } from "./MessageType";
export interface ChatType {
  chatId: string;
  data: {
    length: number;
    messages: MessageType[];
  };
}
