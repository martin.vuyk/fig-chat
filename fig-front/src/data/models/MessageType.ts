export interface MessageType {
  messageId: string;
  who: string;
  what: string;
  when: string;
}
