import { createRouter, createWebHistory } from "vue-router";
import { checkSession } from "./guards/isSessionValid";
import { Route } from "./identify/route";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "",
      component: () => import("./layouts/Login.vue"),
      children: [
        {
          path: "",
          name: Route.Login,
          component: () => import("./layouts/Login.vue"),
        },
      ],
    },
    {
      path: "/Home",
      component: () => import("./layouts/Home.vue"),
      children: [
        {
          name: Route.Home,
          path: "",
          component: () => import("./layouts/Home.vue"),
        },
      ],
      beforeEnter: checkSession,
    },
  ],
});

export default router;
