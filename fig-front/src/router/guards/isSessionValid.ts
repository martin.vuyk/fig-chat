import type { NavigationGuardNext, RouteLocationNormalized } from "vue-router";
import { Route } from "../identify/route";

export const checkSession = (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
) => {
  const hasSession = .getSession() === null ? false : true;
  if (hasSession) {
    next();
  } else {
    next({ name: Route.Login });
  }
};
